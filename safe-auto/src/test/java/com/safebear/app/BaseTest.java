package com.safebear.app;

import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import pages.*;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

/**
 * Created by CCA_Student on 06/11/2017.
 */
public class BaseTest {

    WebDriver driver;
    Utils parameters;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userPage;
    FramesPage framesPage;
    FramesPageMainFrame framesPageMainFrame;


    @Before
    public void setUp() {

        try {
            parameters = new Utils();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.driver = parameters.getDriver();

        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);
        framesPage = new FramesPage(driver);
        framesPageMainFrame = new FramesPageMainFrame(driver);


        driver.get(parameters.getUrl());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();


    }

    @After
    public void tearDown(){


        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}
